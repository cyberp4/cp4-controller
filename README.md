# CyberP4 controller component

## Features

Manages all the hardware interactions :

- bucket stepper motor control
- servos control
- token detectors
- gate opening detector 
- push button

Orchestrates the game play.

Interacts with the AI external service

## Development

Have a look at the Makefile targets for the project build and deployment tasks

## Python dependencies

- Python 3.9.2
- packages : see `requirements.txt` file

## System dependencies

- pigpio
- rabbitmq

## Installation

In the following instructions, the Raspberry Pi hostname is supposed to
be `rpi` and the current user `pi`.

### pigpio

```
# install the package

$ sudo apt-get install pigpio

# setup the systemd service

# - from dev system:

$ scp systemd/pigpiod.service rpi:

# - from rpi:

$ cd /etc/systemd/system
$ sudo ln -s /home/pi/pigpiod.service 
$ sudo systemctl enable pigpiod.service
$ sudo systemctl start pigpiod

# check
$ systemctl status pigpiod
$ journalctl -u pigpiod
```

### RabbitMQ

```
$ sudo apt-get install rabbitmq-server

# the systemd service is automatically configured and started
# => check it
$ systemctl status rabbitmq-server
$ journalctl -u tabbitmq-server

# enable the management console
$ sudo rabbitmq-plugins enable rabbitmq_management

# add the CyberP4 user
$ sudo rabbitmqctl add_user cp4 cp4

# set it as administrator
$ sudo rabbitmqctl set_user_tags cp4 administrator

# give it the required permissions
$ sudo rabbitmqctl set_permissions cp4 ".*" ".*" ".*"
$ sudo rabbitmqctl set_topic_permissions cp4 ".*" ".*" ".*"
```

Note: the permissions are a bit too permissive, and could be restricted for
security's sake. However, no need to be too much paranoïd since this is not
an exposed system.

### CP4 Controller

The wheel file is expected to already in the RPi user home dir.

If not copied from the dev system, create the env vars file:

```
$ echo "CP4_VERBOSE=0" > .env
$ echo "CP4_CONFIGURATION_PATH=/home/pi/config.yml" >> .env
$ echo "CP4_GAME_AI_NETLOC=http://localhost:3150" >> .env
$ echo "CP4_RMQ_USERNAME=cp4" >> .env
$ echo "CP4_RMQ_PASSWORD=cp4" >> .env
```

Note: before deploying the package and configuring the systemd service, 
check that the `cp4-controller.service` file points to this file
in the `EnvironmentFile` setting.

```
# install the package at user level (no sudo)
$ pip install cp4_controller-<version>-py3-none-any.whl

# setup the systemd service
$ cd /etc/system/system
$ sudo ln -s /home/pi/cp4-controller.service
$ sudo systemctl enable cp4-controller

# start it
$ sudo systemctl start cp4-controller

# check all is fine
$ systemctl status cp4-controller
$ journalctl -u cp4-controller
```

### System tuning

It's wise to limit the volume of systemd log files, that can grow very large in case
of service restart loop due to failure. 

This is done by setting the `SystemMaxUse` parameter in `/etc/systemd/journald.conf`.

**Example:**
```
SystemMaxUse=100M
```