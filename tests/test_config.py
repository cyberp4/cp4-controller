from pathlib import Path

import pytest

from cp4_controller.config import ControllerConfiguration

FIXTURES_PATH = Path(__name__).parent / "fixtures"

TOKEN_DETECTOR_GPIOS = (17, 18, 27, 22, 23, 24, 25)


@pytest.fixture()
def config() -> ControllerConfiguration:
    with open(FIXTURES_PATH / "config.yaml") as fp:
        return ControllerConfiguration.from_file(fp)


@pytest.fixture()
def config_overrides() -> ControllerConfiguration:
    with open(FIXTURES_PATH / "config_overrides.yaml") as fp:
        return ControllerConfiguration.from_file(fp)


class TestConfig:
    def test_load_yaml(self):
        with open(FIXTURES_PATH / "config.yaml") as fp:
            cfg = ControllerConfiguration.from_file(fp)

            assert cfg

    def test_general_config(self, config: ControllerConfiguration):
        assert config.gate_sensor_gpio == 16
        assert config.button_gpio == 19

    def test_moving_head_config(self, config: ControllerConfiguration):
        assert config.moving_head

        cfg = config.moving_head
        assert cfg.resetn_gpio == 6
        assert cfg.busyn_gpio == 5
        assert cfg.loading_position == 275
        assert cfg.column_distance == 35
        assert cfg.col1_position == 224

    def test_loader_config(self, config: ControllerConfiguration):
        assert config.loader

        cfg = config.loader
        assert cfg.gpio == 12
        assert cfg.idle_pulse_width == 520
        assert cfg.active_pulse_width == 2250
        assert cfg.delay_for_move_complete == .7

    def test_dropper_config(self, config: ControllerConfiguration):
        assert config.dropper

        cfg = config.dropper
        assert cfg.gpio == 13
        assert cfg.idle_pulse_width == 1000
        assert cfg.active_pulse_width == 2000
        assert cfg.delay_for_move_complete == 0.7

    def test_token_detectors_config(self, config: ControllerConfiguration):
        assert config.token_detectors

        cfg = config.token_detectors
        assert len(cfg) == 7
        for gpio, col_cfg in zip(TOKEN_DETECTOR_GPIOS, cfg):
            assert col_cfg.gpio == gpio
            assert col_cfg.detection_min_ms == 40
            assert col_cfg.detection_max_ms == 150

    def test_get_token_detector_config(self, config: ControllerConfiguration):
        for gpio in TOKEN_DETECTOR_GPIOS:
            assert config.token_detector_config(gpio).gpio == gpio


class TestOverrides:
    overriden = [0, 4]

    def test_default_config(self, config_overrides):
        for ndx, cfg in enumerate(config_overrides.token_detectors):
            if ndx in self.overriden:
                assert cfg.detection_min_ms == 50
                assert cfg.detection_max_ms == 160
                assert cfg.token_debounce_ms == 15
            else:
                assert cfg.detection_min_ms == 40
                assert cfg.detection_max_ms == 150
                assert cfg.token_debounce_ms == 10
