import pytest

from cp4_controller.board import Board, BOARD_SIZE, ROWS_COUNT, COLUMNS_COUNT, Player

__author__ = 'Eric Pascual'


@pytest.fixture()
def board() -> Board:
    return Board()


class TestBoard:
    def test_as_string(self, board: Board):
        s = board.to_string()
        assert s == '0' * BOARD_SIZE

    def test_as_string_by_row(self, board: Board):
        s = board.to_string(by_row=True)
        assert s == '0' * BOARD_SIZE

    def test_play(self, board: Board):
        board.update(Player.HUMAN, 1)

        assert board.get_column_height(1) == 1
        assert not board.column_is_full(1)

    @pytest.mark.parametrize(
        'column, error_expected', [
            (1, False),
            (2, False),
            (3, False),
            (4, False),
            (5, False),
            (6, False),
            (7, False),
            (0, True),
            (8, True),
            (-1, True),
        ]
    )
    def test_check_column_num(self, board: Board, column: int, error_expected: bool):
        if error_expected:
            with pytest.raises(ValueError):
                print(board.get_column_height(column))

        else:
            _ = board.get_column_height(column)

    def test_column_is_full(self, board: Board):
        for _ in range(ROWS_COUNT):
            board.update(Player.HUMAN, 1)

        assert board.column_is_full(1)

        with pytest.raises(ValueError):
            board.update(Player.HUMAN, 1)

    def test_player_has_four_connected_vert(self, board: Board):
        for _ in range(3):
            board.update(Player.HUMAN, 1)
            assert not board.player_has_four_connected(Player.HUMAN)

        board.update(Player.HUMAN, 1)
        assert board.player_has_four_connected(Player.HUMAN)

    def test_player_has_four_connected_horiz(self, board: Board):
        for col in range(1, 5):
            board.update(Player.HUMAN, col)

        assert board.get_winner() == Player.HUMAN

    def test_player_has_four_connected_diag1(self, board: Board):
        for col in range(1, 4):
            board.update(Player.HUMAN, col)

            for _ in range(col):
                board.update(Player.MACHINE, col + 1)

            assert board.get_winner() is None

        board.update(Player.HUMAN, 4)
        assert board.get_winner() == Player.HUMAN

    def test_player_has_four_connected_diag2(self, board: Board):
        for col in range(4, 1, -1):
            board.update(Player.HUMAN, col)

            for _ in range(5 - col):
                board.update(Player.MACHINE, col - 1)

            assert board.get_winner() is None

        board.update(Player.HUMAN, 1)
        assert board.get_winner() == Player.HUMAN

    def test_get_winner(self, board: Board):
        for _ in range(3):
            board.update(Player.HUMAN, 1)
            assert board.get_winner() is None

        board.update(Player.HUMAN, 1)
        assert board.get_winner() == Player.HUMAN

    def test_player_get_four_connected_horiz(self):
        for row in range(1, 3):
            for start_col in range(1, 5):
                board = Board()
                # fill rows below with a pattern not containing alignements already
                for r in range(1, row):
                    for c in range(1, COLUMNS_COUNT + 1):
                        board.update(Player.HUMAN if (r * COLUMNS_COUNT + c) % 2 else Player.MACHINE, c)

                col = start_col
                for _ in range(3):
                    board.update(Player.HUMAN, col)
                    assert board.get_four_connected(Player.HUMAN) is None
                    col += 1

                board.update(Player.HUMAN, col)
                c4 = board.get_four_connected(Player.HUMAN)
                assert c4
                assert c4 == (start_col, row, 1, 0)

    def test_player_get_four_connected_vert(self):
        for row in range(1, 3):
            for col in range(1, COLUMNS_COUNT + 1):
                board = Board()
                # fill rows below with a pattern not containing alignements already
                for r in range(1, row):
                    board.update(Player.MACHINE, col)

                for _ in range(3):
                    board.update(Player.HUMAN, col)
                    c4 = board.get_four_connected(Player.HUMAN)
                    assert c4 is None

                board.update(Player.HUMAN, col)
                c4 = board.get_four_connected(Player.HUMAN)
                assert c4 == (col, row, 0, 1)

    def test_player_get_four_connected_diag_pos(self, board: Board):
        for col in range(1, 4):
            board.update(Player.HUMAN, col)

            for _ in range(col):
                board.update(Player.MACHINE, col + 1)

            c4 = board.get_four_connected(Player.HUMAN)
            assert c4 is None

        board.update(Player.HUMAN, 4)
        c4 = board.get_four_connected(Player.HUMAN)
        assert c4 == (1, 1, 1, 1)

    def test_player_get_four_connected_diag_neg(self, board: Board):
        for col in range(4, 1, -1):
            board.update(Player.HUMAN, col)

            for _ in range(5 - col):
                board.update(Player.MACHINE, col - 1)

            c4 = board.get_four_connected(Player.HUMAN)
            assert c4 is None

        board.update(Player.HUMAN, 1)

        # s = board.to_string(by_row=True)
        # for start in range(0, BOARD_SIZE, COLUMNS_COUNT):
        #     print(s[start:start + COLUMNS_COUNT])

        c4 = board.get_four_connected(Player.HUMAN)
        assert c4 == (1, 4, 1, -1)
