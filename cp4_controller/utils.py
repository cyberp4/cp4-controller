from typing import Tuple

from .app_types import Player, C4Bitmap
from .board import COLUMNS_COUNT, ROWS_COUNT


def board_string_to_bitmaps(board: str, player: Player) -> Tuple[C4Bitmap, C4Bitmap]:
    """ Return the position and mask bitmaps for a given player.

    The `board` argument is a 42 characters string representing the content of
    the board rasterized line by line. Characters can be:
    - 'n' : None, aka the cell is empty
    - 'h' : token by the human
    - 'm' : token played by the machine

    The sequence starts in the bottom-left corner and goes upto the top-right one.

    The bitmaps are bit strings in which the LSB is the occupancy of the bottom left
    cell, and the MSB is the same for the top right one. Scanning is done by column first,
    that means that a sequence of 7 bits represent the content of a  column. Each column
    contains an additional bit always set to 0 and acting as a sentinel (i.e. separator of
    consecutive columns). This is needed to implement tests and moves as fast bitwise
    operations while avoiding the wrapping effect that would be caused by the absence of the
    column separator.

    The `position` bitmap represents the cells where a player has a token. The `mask`
    bitmap represents the cells where there is a token, whichever is its owner. Obtaining
    the opponent's position is thus as simple as XORing `position` and `mask`.

    The `player` parameter is the one for which the position bitmap is returned.

    The result is a tuple containing the bitmaps.
    """
    position, mask = '', ''

    # since we are going to build the bit string by incrementally appending its pieces,
    # and hence virtually starting with the MSB one, we need to scan the board starting
    # from the rightmost column
    for j in range(COLUMNS_COUNT - 1, -1, -1):
        mask += '0'  # start with the sentinel, so that it will result on top of the column
        position += '0'  # same
        for i in range(ROWS_COUNT - 1, -1, -1):
            cell = board[i * COLUMNS_COUNT + j]
            mask += '1' if cell != 'n' else '0'
            position += '1' if cell == player.code else '0'

    return int(position, 2), int(mask, 2)


_true_values = {'true', '1', 'on', 'yes'}


def to_bool(value) -> bool:
    if isinstance(value, bool):
        return value

    if isinstance(value, int):
        return value != 0

    if isinstance(value, str):
        return value.lower() in _true_values

    raise ValueError()
