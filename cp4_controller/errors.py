# -*- coding: utf-8 -*-

__author__ = 'Eric Pascual'


class ServiceError(Exception):
    pass


class ServiceControllerError(ServiceError):
    pass


class ServiceAMQPError(ServiceError):
    def __init__(self, amqp_error):
        self.amqp_error = amqp_error
