from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import logging
import json

from .utils import to_bool
from .controller import Controller, ControllerState
from .board import Board, validated_column, COLUMNS_COUNT
from .app_types import Game
from .log import LogMgr
from .errors import ServiceError

__author__ = 'Eric Pascual'


class HttpRequestError(ServiceError):
    def __init__(self, status, detail):
        self.status = status
        self.detail = detail


class CP4HttpServer(ThreadingMixIn, HTTPServer):
    def __init__(self, listening_port: int, controller: Controller, board: Board):
        super().__init__(server_address=('', listening_port), RequestHandlerClass=CP4HTTPRequestHandler)
        self.controller = controller
        self.board = board
        self.logger = LogMgr.app_logger.getChild('http')


class CP4HTTPRequestHandler(BaseHTTPRequestHandler):
    # attributes set by setup() method during the instance creation
    # (defined here to let type checking do its job without throwing errors or warnings,
    # and also for documentation's sake)
    controller: Controller
    board: Board
    logger: logging.Logger
    game: Game

    def do_method(self, method: str):
        if '?' in self.path:
            path, qry_string = self.path.split('?')
            qry = dict(part.split('=') for part in qry_string.split('&')) or dict()

        else:
            path, qry = self.path, dict()

        end_point = path.strip('/').replace('/', '_')

        self.logger.info(f"{method=} {end_point=} {qry=}")

        if handler := getattr(self, f"{method.lower()}_{end_point}", None):
            try:
                response_data = handler(qry)

            except HttpRequestError as http_err:
                self.send_response(http_err.status)
                self.send_header('Content-Type', "application/json")
                self.end_headers()
                self.wfile.write(json.dumps({"detail": str(http_err.detail)}).encode())

            except Exception as unexp_err:
                self.send_response(422)
                self.send_header('Content-Type', "application/json")
                self.end_headers()
                self.wfile.write(json.dumps({
                    "error": unexp_err.__class__.__name__,
                    "detail": str(unexp_err)
                }).encode())

            else:
                self.send_response(200)
                self.send_header('Content-Type', "application/json")
                self.end_headers()

                if response_data:
                    self.wfile.write(json.dumps(response_data).encode())

        else:
            self.send_response(404)
            self.end_headers()

    def do_GET(self):
        return self.do_method('GET')

    def do_POST(self):
        return self.do_method('POST')

    def do_PUT(self):
        return self.do_method('POST')

    def setup(self) -> None:
        super().setup()

        server: CP4HttpServer = self.server  # type: ignore
        self.controller = server.controller
        self.board = server.board
        self.logger = server.logger
        self.game = Game()

    def get_status(self, qry: dict):
        fake_mode = self.controller.head_driver.fake_mode
        dspin_report = {
            "fake_mode": fake_mode
        }
        if not fake_mode:
            carriage = self.controller.head_driver
            dspin_report.update({
                "config": hex(carriage.get_config()),
                "status": hex(carriage.get_status()),
                "calibrated": carriage.is_calibrated
            })
        return {
            "status": "OK",
            "dspin": dspin_report
        }

    def get_board(self, qry: dict):
        by_row = to_bool(qry.get('by_row', False))
        return {
            "board": self.board.to_string(by_row=by_row)
        }

    def get_display(self, qry: dict):
        s = self.board.to_string(by_row=True)
        args = [iter(s)] * COLUMNS_COUNT
        return [''.join(line) for line in zip(*args)]

    def get_state(self, qry: dict):
        return {
            "state": self.controller.state.name
        }

    def get_col_height(self, qry: dict):
        try:
            column = validated_column(int(qry["col"]))
        except KeyError:
            raise HttpRequestError(400, "missing column number")
        except ValueError as e:
            raise HttpRequestError(400, e)

        return {
            "height": self.board.get_column_height(column)
        }

    def post_reset(self, qry: dict):
        self.board.clear()
        self.controller.start_game()
        self.game = Game()

    def post_ctrl_play(self, qry: dict):
        if self.controller.state == ControllerState.GAME_OVER:
            raise HttpRequestError(422, "game is over")

        if self.controller.state != ControllerState.PLAYING:
            raise HttpRequestError(422, "not playing a game")

        try:
            column = validated_column(int(qry["col"]))
        except ValueError as e:
            raise HttpRequestError(400, e)
        except KeyError:
            raise HttpRequestError(400, "missing column number")

        if self.board.column_is_full(column):
            raise HttpRequestError(422, "column is full")

        self.controller.play_token_in_column(column)

    def post_ctrl_cal(self, qry: dict):
        self.controller.calibrate_moving_head()

    def post_ctrl_dis(self, qry: dict):
        self.controller.disable_head()

    def post_ctrl_prep(self, qry: dict):
        self.controller.calibrate_moving_head()
        self.controller.go_to_loading_position()

    def post_ctrl_refill(self, qry: dict):
        self.controller.go_to_tank_refilling_position()

    def post_ctrl_goto(self, qry: dict):
        try:
            column = validated_column(int(qry["col"]))
        except KeyError:
            raise HttpRequestError(400, "missing column number")
        except ValueError as e:
            raise HttpRequestError(400, e)

        self.controller.go_to_column(column)

    def post_ctrl_goto_load(self, qry: dict):
        self.controller.go_to_loading_position()

    def post_ctrl_dropper_open(self, qry: dict):
        self.controller.token_dropper.activate()

    def post_ctrl_dropper_close(self, qry: dict):
        self.controller.token_dropper.idle()

    def post_ctrl_dropper_actuate(self, qry: dict):
        self.controller.token_dropper.actuate()

    def post_ctrl_loader_push(self, qry: dict):
        self.controller.token_loader.activate()

    def post_ctrl_loader_retract(self, qry: dict):
        self.controller.token_loader.idle()

    def post_ctrl_loader_actuate(self, qry: dict):
        self.controller.token_loader.actuate()

    def get_winner(self, qry: dict):
        return {
            "winner": self.game.winner
        }

    def get_sensor_tokens(self, qry: dict):
        return {
            "state": self.controller.get_token_detectors_state()
        }

    def get_sensor_gate(self, qry: dict):
        return {
            "state": self.controller.get_gate_state()
        }
