import time

import pigpio

from .config import ServoConfiguration


class NoopLogger:
    def noop(self, *args, **kwargs):
        pass

    info = warning = error = debug = exception = noop


class Servo:
    def __init__(self, rpi: pigpio.pi, config: ServoConfiguration, logger=None):
        self.rpi = rpi
        self.gpio = config.gpio
        self.idle_pulse_width = config.idle_pulse_width
        self.active_pulse_width = config.active_pulse_width
        self.delay_for_move_complete = config.delay_for_move_complete
        self.logger = logger or NoopLogger()
        self.fake_rpi = getattr(self.rpi, "fake", False)

        self.disable()

    def disable(self):
        self.logger.debug("set_servo_pulsewidth(%d)", 0)
        self.rpi.set_servo_pulsewidth(self.gpio, 0)

    def ready(self):
        self.idle()

    def _move_servo(self, pulse, delay: float = 0, disable_after=True):
        self.logger.debug("set_servo_pulsewidth(%d, %.3f, %s)", pulse, delay, disable_after)
        self.rpi.set_servo_pulsewidth(self.gpio, pulse)
        if delay:
            if not self.fake_rpi:
                time.sleep(delay)
            else:
                self.logger.debug("mocked delay (%d ms)", delay * 1000)
            if disable_after:
                self.disable()

    def activate(self, wait=True, disable_after=True):
        self._move_servo(
            self.active_pulse_width,
            self.delay_for_move_complete if wait else 0,
            disable_after
        )

    def idle(self, wait=True, disable_after=True):
        self.logger.info("idle()")
        self._move_servo(
            self.idle_pulse_width,
            self.delay_for_move_complete if wait else 0,
            disable_after
        )

    def actuate(self, wait=True):
        self.logger.info("actuate()")
        self.activate(disable_after=False)
        self.idle(wait=wait)
