import logging

logger = logging.getLogger('cp4.pigpio')
logger.warning('using fake pigpio package')

PUD_DN = 0
PUD_UP = 1

INPUT = 0
OUTPUT = 1

RISING_EDGE = 0
FALLING_EDGE = 1
EITHER_EDGE = 2


class MockedCallback:
    def __init__(self, gpio):
        self.gpio = gpio

    def cancel(self):
        logger.debug(f"MockedCallback[{self.gpio=}].cancel()")


class pi:
    fake = True

    def set_servo_pulsewidth(self, gpio: int, pulse: int):
        logger.debug(f"mocked set_servo_pulsewidth(gpio={gpio}, pulse={pulse})")

    def callback(self, gpio: int, edge: int, func) -> MockedCallback:
        logger.debug(f"mocked callback({gpio=}, {edge=}, {func=})")
        return MockedCallback(gpio)

    def spi_xfer(self, handle, data):
        logger.debug(f"mocked spi_xfer({handle=}, {data=})")
        bytes_cnt = len(data)
        return bytes, b'0' * bytes_cnt

    def set_pull_up_down(self, gpio: int, state: int):
        logger.debug(f"mocked set_pull_up_down({gpio=}, {state=})")

    def set_glitch_filter(self, gpio: int, delay: int):
        logger.debug(f"mocked set_glitch_filter({gpio=}, {delay=})")

    def set_mode(self, gpio: int, mode: int):
        logger.debug(f"mocked set_mode({gpio=}, {mode=})")

    def write(self, gpio: int, value: int):
        logger.debug(f"mocked write({gpio=}, {value=})")

    def read(self, gpio: int):
        result = 0
        logger.debug(f"mocked read({gpio=}) -> {result}")
        return result

    def spi_open(self, *args):
        logger.debug(f"mocked spi_open({args=})")
        return 42

    def spi_close(self, handle: int):
        logger.debug(f"mocked spi_close({handle=})")
