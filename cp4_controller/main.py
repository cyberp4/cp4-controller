#!/usr/bin/env python

import argparse
import dataclasses
import logging
import sys
from pathlib import Path
import json

try:
    model = Path("/sys/firmware/devicetree/base/model").read_text()
    is_raspberrypi = "raspberry" in model.lower()
except IOError:
    is_raspberrypi = False

if is_raspberrypi:
    import pigpio
else:
    from cp4_controller import fake_pigpio as pigpio

from cp4_controller.controller import ControllerConfiguration
from cp4_controller.dspin import MovingHeadDriver
from cp4_controller.service import Service
from cp4_controller.errors import ServiceError, ServiceAMQPError
from cp4_controller.config import load_settings_from_env
from cp4_controller.log import LogMgr


def main():
    settings = load_settings_from_env()

    parser = argparse.ArgumentParser(description="CyberP4 Controller Service")
    parser.add_argument('--verbose', '-v', action='store_true')

    args = parser.parse_args()

    verbose = settings.verbose or args.verbose
    app_logger = LogMgr.setup(verbose)

    with settings.configuration_path.open() as config_file:
        controller_config = ControllerConfiguration.from_file(config_file)
        cfg_report = json.dumps(dataclasses.asdict(controller_config), indent=2)
        app_logger.info("Configuration loaded from %s", config_file.name)
        for line in cfg_report.splitlines():
            app_logger.info("| %s", line)
        app_logger.info("---")

    rpi = pigpio.pi()

    head_logger = app_logger.getChild('dspin')
    head_logger.setLevel(logging.DEBUG if settings.dspin_trace_spi else logging.INFO)

    head_driver = MovingHeadDriver(
        rpi,
        config=controller_config.moving_head,
        logger=head_logger,
        fake_mode=settings.dspin_fake_mode
    )

    try:
        with Service(rpi, head_driver, controller_config, settings) as service:
            service.run()

    except ServiceAMQPError as e:
        app_logger.error('unable to start due to AMQP error: %r', e.amqp_error)
        sys.exit(1)

    except ServiceError as e:
        app_logger.error('unexpected service error: %r', e)
        sys.exit(1)

    finally:
        app_logger.info("terminated.")


if __name__ == '__main__':
    main()
