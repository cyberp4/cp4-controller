import time

import pigpio

from .config import MovingHeadConfiguration


class DSPinError(Exception):
    pass


class DSpin:
    """ pigpio based DSpin model.

    Implements a context manager for properly manage the chip state and
    the associated resources. This allows writing code such as :

        with DSPin(pi, RESETN_PIN, BUSYN_PIN) as dsp:
            ...
    """
    MAX_SPEED = 0xff
    ACC = 0x8a
    STEP_MODE = 7
    HOME_SEEK_SPEED = 0xfff

    def __init__(self, rpi: pigpio.pi, resetn_gpio: int, busyn_gpio: int, logger):
        self.rpi = rpi
        self.resetn = resetn_gpio
        self.busyn = busyn_gpio

        self.spi_h = None
        self.logger = logger
        self.is_calibrated = False

    def setup(self):
        """ Configures the GPIOs used for the control signals and the SPI bus,
         awake the chip and check it is OK.

         Raises a DSPinError if a failure occurs.

         Note: the method does nothing if the SPI bus is already opened
        """
        if self.spi_h:
            return

        self.rpi.set_mode(self.resetn, pigpio.OUTPUT)
        self.rpi.set_mode(self.busyn, pigpio.INPUT)

        self.rpi.set_mode(self.resetn, pigpio.OUTPUT)
        self.rpi.set_mode(self.busyn, pigpio.INPUT)

        # awake the chip by toggling the RESETN line
        self.rpi.write(self.resetn, 0)
        self.rpi.write(self.resetn, 1)
        self.rpi.write(self.resetn, 0)
        self.rpi.write(self.resetn, 1)

        try:
            # 300_000 results in a clock with a real frequency just under 500 kHz
            # (measured with a logical analyzer)
            self.spi_h = self.rpi.spi_open(0, 300_000, 3)
        except pigpio.error as e:
            raise DSPinError(f'cannot open SPI: {e}')

        # check the connection by comparing the current configuration with the
        # reset one
        if (config_data := self.get_config()) != 0x2e88:
            self.logger.error("bad configuration at start: 0x%x (expected: 0x2e88)", config_data)
            self.teardown()
            raise DSPinError("reset configuration mismatch")

        # set ACC
        self.set_param(0x05, 2, self.ACC)

        # set MAX_SPEED
        self.set_param(0x07, 2, self.MAX_SPEED)

        # set STEP_MODE
        self.set_param(0x16, 1, self.STEP_MODE)

        self.is_calibrated = False

    def teardown(self):
        """ Performs the final housekeeping by shutting the dSPIN down,
        closing the SPI bus and disabling the chip.

        Does nothing if the SPI bus is not opened.
        """
        if not self.spi_h:
            return

        self.disable()

        self.rpi.spi_close(self.spi_h)
        self.spi_h = None

        self.rpi.write(self.resetn, 0)

    def __enter__(self):
        self.setup()
        return self

    def __exit__(self, *args, **kwargs):
        self.teardown()

    def _log_xfer(self, rx: bool, data):
        self.logger.debug(
            "%s %s",
            "->" if rx else "<-",
            " ".join(f"{b:02x}" for b in data)
        )

    def send_command(self, command: int, args=None):
        if self.spi_h is None:
            raise DSPinError("setup() must be called before any operation")

        if command != command & 0xff:
            raise ValueError("invalid command")
        if args and any(arg != arg & 0xff for arg in args):
            raise ValueError("invalid args")

        buffer = [command] + (args or [])
        self._log_xfer(True, buffer)

        resp = b''.join(self.rpi.spi_xfer(self.spi_h, [b])[1] for b in buffer)
        self._log_xfer(False, resp)

        return resp[1:] if args else None

    def get_config(self):
        resp = self.send_command(0x38, [0, 0])
        return (resp[0] << 8) | resp[1]

    def get_status(self):
        resp = self.send_command(0xd0, [0, 0])
        return (resp[0] << 8) | resp[1]

    def disable(self):
        self.send_command(0xa8)

    def check_calibration(self):
        if not self.is_calibrated:
            raise DSPinError("not calibrated yet")

    def wait_end_of_move(self):
        if getattr(self.rpi, 'fake', False):
            return

        while self.rpi.read(self.busyn) == 0:
            time.sleep(0)

    def move(self, steps: int, forward=True, wait=True):
        """ Relative move.
        """
        self.logger.info("move'(steps=%d forward=%s)'", steps, forward)
        steps = int(steps)
        self.send_command(
            0x41 if forward else 0x40,
            [
                (steps >> 16) & 0x3f, (steps >> 8) & 0xff, steps & 0xff
            ]
        )
        if wait:
            self.wait_end_of_move()

    def go_to(self, abs_pos: int, wait=True, disable_after=True):
        """ Absolute move.
        """
        self.logger.info("go_to(abs_pos=%d)", abs_pos)
        self.check_calibration()
        self.send_command(
            0x60,
            [
                (abs_pos >> 16) & 0x3f, (abs_pos >> 8) & 0xff, abs_pos & 0xff
            ]
        )
        if wait:
            self.wait_end_of_move()
            if disable_after:
                self.disable()

    def go_home(self, wait=True, disable_after=True):
        """ Return to home position (i.e. internal step counter = 0).
        """
        self.logger.info("go_home()")
        self.check_calibration()
        self.send_command(0x70)
        if wait:
            self.wait_end_of_move()
            if disable_after:
                self.disable()

    def go_until_switch(self, speed: int = None, forward=False, wait=True):
        """ Moves until the end-switch is actuated.
        """
        self.logger.info("go_until_switch(speed=%s, forward=%s)", speed, forward)
        speed = int(speed) if speed else self.HOME_SEEK_SPEED
        self.send_command(
            0x83 if forward else 0x82,
            [
                (speed >> 16) & 0x0f, (speed >> 8) & 0xff, speed & 0xff
            ]
        )
        if wait:
            self.wait_end_of_move()

    def switch_is_closed(self):
        """ Returns if the end-switch is closed or not.
        """
        status = self.get_status()
        return status & 0x04

    def release_switch(self, wait=True):
        """ Moves until the end-switch is released.
        """
        self.logger.info("release_switch()")
        if self.switch_is_closed():
            self.send_command(0x93)
            if wait:
                self.wait_end_of_move()

    def calibrate(self, speed: int = None, forward=False, disable_after=True):
        self.logger.info("calibrate(speed=%s, forward=%s, disable_after=%s)", speed, forward, disable_after)
        if not self.switch_is_closed():
            self.go_until_switch(speed, forward)
        self.release_switch()

        self.is_calibrated = True
        if disable_after:
            self.disable()

    def get_param(self, param, size):
        """ Reads the value of a given configuration parameter.
        """
        resp = self.send_command(0x20 | param, [0] * size)
        value = 0
        for i in range(size):
            value = (value << 8) | resp[i]
        return value

    def set_param(self, param, size, value):
        """ Writes the value of a given configuration parameter.
        """
        buffer = [0] * size
        for i in reversed(range(size)):
            buffer[i] = value & 0xff
            value = value >> 8
        self.send_command(param, buffer)


class MovingHeadDriver(DSpin):
    MSTEPS_PER_MM = 302.2
    COLS_COUNT = 7
    ACC = 0x6a
    MAX_SPEED = 0xff
    STEP_MODE = 7
    HOME_SEEK_SPEED = 0xfff

    def __init__(self, rpi: pigpio.pi, config: MovingHeadConfiguration, logger=None, fake_mode=False):
        super().__init__(rpi, resetn_gpio=config.resetn_gpio, busyn_gpio=config.busyn_gpio, logger=logger)

        self.fake_mode = fake_mode
        self.config = config

    def _steps(self, mm: float) -> int:
        return int(mm * self.MSTEPS_PER_MM)

    def get_config(self) -> int:
        if self.fake_mode:
            return 0x2e88
        return super().get_config()

    def go_to_column(self, col: int, wait=True, disable_after=True):
        if not 1 <= col <= self.COLS_COUNT:
            raise ValueError("invalid column")

        steps = self._steps(self.config.col1_position - (col - 1) * self.config.column_distance)
        self.go_to(steps, wait)
        if disable_after:
            self.disable()

    def go_to_loading_position(self, wait=True):
        self.go_to(self._steps(self.config.loading_position), wait)
        self.disable()
