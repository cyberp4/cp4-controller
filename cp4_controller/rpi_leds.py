import abc
from enum import Enum
from pathlib import Path

from .log import LogMgr

try:
    model = Path("/sys/firmware/devicetree/base/model").read_text()
    is_raspberrypi = "raspberry" in model.lower()

    # check the pseudo files name since this has changed in recent versions
    uses_ACT_PWR = Path("/sys/class/leds/ACT").exists()

except IOError:
    is_raspberrypi = False
    uses_ACT_PWR = False


class LEDMode(str, Enum):
    NONE = 'none'
    OFF = 'none'
    ON = 'default-on'
    HEARTBEAT = 'heartbeat'
    BLINK = 'timer'
    MMC_IO = 'mmc0'
    POWER = 'input'


class RPILed(abc.ABC):
    class Meta:
        pseudo_file: str
        default_mode: LEDMode

    @classmethod
    def _sys_file_path(cls) -> Path:
        return Path(f"/sys/class/leds/{cls.Meta.pseudo_file}/trigger")

    @classmethod
    def _set_trigger(cls, mode: LEDMode):
        if is_raspberrypi:
            cls._sys_file_path().write_text(str(mode.value))
        else:
            logger = LogMgr.app_logger.getChild('led')
            logger.warning("not on RPi => set trigger ignored")

    @classmethod
    def restore_default(cls):
        cls._set_trigger(cls.Meta.default_mode)

    @classmethod
    def set_mode(cls, mode: LEDMode):
        cls._set_trigger(mode)


class LedPower(RPILed):
    class Meta(RPILed.Meta):
        pseudo_file = "PWR" if uses_ACT_PWR else "led1"
        default_mode = LEDMode.POWER


class LedActivity(RPILed):
    class Meta(RPILed.Meta):
        pseudo_file = "ACT" if uses_ACT_PWR else "led0"
        default_mode = LEDMode.MMC_IO
