from dataclasses import dataclass, field, asdict
from typing import List, TextIO
from pathlib import Path

import yaml

from .board import COLUMNS_COUNT


# @dataclass
@dataclass
class TokenDetectorDelaysMixin:
    detection_min_ms: int = 0
    detection_max_ms: int = 0
    token_debounce_ms: int = 0

    def __post_init__(self):
        if self.detection_min_ms and self.detection_max_ms and self.detection_min_ms >= self.detection_max_ms:
            raise ValueError(f'invalid detection delays ({self.detection_min_ms}, {self.detection_max_ms})')

        if self.token_debounce_ms and self.detection_max_ms and self.token_debounce_ms > self.detection_min_ms:
            raise ValueError(
                f'debounce delay cannot be greater than pulse min ({self.token_debounce_ms}, {self.detection_min_ms})'
            )


@dataclass
class TokenDetectorConfiguration(TokenDetectorDelaysMixin):
    gpio: int = 0

    def __post_init__(self):
        super().__post_init__()

        if self.gpio == 0:
            raise ValueError(f'gpio is mandatory')

        if self.gpio not in (17, 18, 22, 23, 24, 25, 27):
            raise ValueError(f'invalid gpio number ({self.gpio})')


@dataclass
class TokenDetectorDefaults(TokenDetectorDelaysMixin):
    pass


@dataclass
class MovingHeadConfiguration:
    resetn_gpio: int
    busyn_gpio: int
    loading_position: int  # mm
    column_distance: int  # mm
    col1_position: int  # mm


@dataclass
class ServoConfiguration:
    gpio: int
    idle_pulse_width: int
    active_pulse_width: int  # msecs
    delay_for_move_complete: float  # secs


@dataclass
class ControllerConfiguration:
    moving_head: MovingHeadConfiguration
    loader: ServoConfiguration
    dropper: ServoConfiguration
    token_detectors: List[TokenDetectorConfiguration]
    gate_sensor_gpio: int
    button_gpio: int

    _gpio_to_col_ndx: dict[int, int] = field(init=False)

    @classmethod
    def from_file(cls, fp: TextIO) -> "ControllerConfiguration":
        data = yaml.load(fp, yaml.Loader)

        token_detector_defaults = TokenDetectorDefaults(**(data['token_detector_defaults']))

        token_detectors = [
            TokenDetectorConfiguration(**{**asdict(token_detector_defaults), **td_data})
            for td_data in data['token_detectors']
        ]
        return ControllerConfiguration(
            moving_head=MovingHeadConfiguration(**data['moving_head']),
            loader=ServoConfiguration(**data['loader']),
            dropper=ServoConfiguration(**data['dropper']),
            token_detectors=token_detectors,
            gate_sensor_gpio=data['gate_sensor_gpio'],
            button_gpio=data['button_gpio'],
        )

    def __post_init__(self):
        detector_count = len(self.token_detectors)
        if detector_count != COLUMNS_COUNT:
            raise ValueError(
                f'invalid detector configuration list length (expected:{COLUMNS_COUNT} got:{detector_count})')

        self._gpio_to_col_ndx = {}
        for col_index, cfg in enumerate(self.token_detectors):
            self._gpio_to_col_ndx[cfg.gpio] = col_index

    def token_detector_config(self, gpio: int) -> TokenDetectorConfiguration:
        return self.token_detectors[self._gpio_to_col_ndx[gpio]]


@dataclass
class ServiceSettings:
    verbose: bool = False
    dspin_fake_mode: bool = False
    dspin_trace_spi: bool = False
    trace_actuators: bool = False
    listening_port: int = 3142
    game_ai_netloc: str = ''
    configuration_path: Path = Path('/etc/cyberp4/config.yaml')
    rmq_username: str = 'guest'
    rmq_password: str = 'guest'
    poweroff_dry_run: bool = False

    @classmethod
    def from_env(cls, env) -> 'ServiceSettings':
        with env.prefixed('CP4_'):
            return ServiceSettings(
                verbose=env.bool('VERBOSE', False),
                dspin_fake_mode=env.bool('DSPIN_FAKE_MODE', False),
                dspin_trace_spi=env.bool('DSPIN_TRACE_SPI', False),
                trace_actuators=env.bool('CP4_TRACE_ACTUATORS', False),
                listening_port=env.int('LISTENING_PORT', 3142),
                game_ai_netloc=env('GAME_AI_NETLOC', "http://localhost:3143"),
                configuration_path=env.path("CONFIGURATION_PATH"),
                rmq_username=env('RMQ_USERNAME', 'guest'),
                rmq_password=env('RMQ_PASSWORD', 'guest'),
                poweroff_dry_run=env.bool('POWEROFF_DRY_RUN', False)
            )


def load_settings_from_env() -> ServiceSettings:
    from environs import Env
    env = Env()
    env.read_env()

    return ServiceSettings.from_env(env)
