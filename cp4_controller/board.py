from typing import Union
import math

from .app_types import Player, C4Bitmap

COLUMNS_COUNT = 7
ROWS_COUNT = 6
COLUMNS_HEIGHT = ROWS_COUNT
BOARD_SIZE = COLUMNS_COUNT * ROWS_COUNT

BOARD_MASK = int(('0' + '1' * ROWS_COUNT) * COLUMNS_COUNT, 2)

BITMAP_HEIGHT = ROWS_COUNT + 1  # to take the sentinel row into account
BITMAP_SIZE = BITMAP_HEIGHT * COLUMNS_COUNT

FILLED_BOARD = int(('0' + '1' * ROWS_COUNT) * COLUMNS_COUNT, 2)


def validated_column(c: int) -> int:
    if 1 <= c <= COLUMNS_COUNT:
        return c

    raise ValueError(f"invalid column number ({c}")


class Board:
    """ Grid positions occupancy is encoded as a bitstring which LSB represents
    the bottom left position of the physical grid.

    Each column has an additional empty bit on top of it and set to 0, acting as a sentinel
    in the connections search process.
    """

    def __init__(self):
        self._occupied: C4Bitmap = 0
        self._machine: C4Bitmap = 0
        self._human: C4Bitmap = 0

        self._column_height = [0] * COLUMNS_COUNT

    def clear(self):
        self._occupied = self._machine = self._human = 0
        self._column_height = [0] * COLUMNS_COUNT

    def update(self, player: Player, column: int):
        """ Update the board state with a given play.
        """
        validated_column(column)

        if self._column_height[column - 1] == ROWS_COUNT:
            raise ValueError('column is already full')

        # The token is placed at the bottom of the target column by the 1-shift.
        # It is then stacked to its top thanks to the way the arithmetic sum works in binary and
        # to the fact that there are no "hole" in the middle of token stacks.

        self._occupied = self._occupied | (self._occupied + (1 << ((column - 1) * BITMAP_HEIGHT)))

        if player == Player.MACHINE:
            self._machine = self._occupied ^ self._human
        else:
            self._human = self._occupied ^ self._machine

        self._column_height[column - 1] += 1

    def column_is_full(self, column: int) -> bool:
        return self.get_column_height(column) == ROWS_COUNT

    def is_full(self) -> bool:
        return all(h == ROWS_COUNT for h in self._column_height)

    def get_column_height(self, column: int):
        validated_column(column)

        return self._column_height[column - 1]

    @staticmethod
    def has_four_connected(position: C4Bitmap) -> bool:
        """ Detects if any of the possible 4 tokens alignment is present in
        a given position bitmap.

        Using appropriate shifts and bitwise arithmetics avoids expensive loops
        through the board.
        """
        # Horizontal check
        m = position & (position >> BITMAP_HEIGHT)
        if m & (m >> 2 * BITMAP_HEIGHT):
            return True

        # Diagonal \
        m = position & (position >> (BITMAP_HEIGHT - 1))
        if m & (m >> 2 * (BITMAP_HEIGHT - 1)):
            return True

        # Diagonal /
        m = position & (position >> (BITMAP_HEIGHT + 1))
        if m & (m >> 2 * (BITMAP_HEIGHT + 1)):
            return True

        # Vertical
        m = position & (position >> 1)
        if m & (m >> 2):
            return True

        # Nothing found
        return False

    def get_four_connected(self, player: Player) -> Union[tuple[int, int, int, int], None]:
        """ An extended version of has_four_connected that returns the detail of
        the series if any.

        The alignment is a 4-items tuple (start_col, start_row, dir_col, dir_row),
        dir_xxx being the coordinates of the direction line.
        """
        position = self._human if player == Player.HUMAN else self._machine

        # Horizontal check
        m = position & (position >> BITMAP_HEIGHT)
        m &= (m >> 2 * BITMAP_HEIGHT)
        if m:
            start = int(math.log(m, 2))
            return 1 + start // BITMAP_HEIGHT, 1 + start % BITMAP_HEIGHT, 1, 0

        # Diagonal \
        m = position & (position >> (BITMAP_HEIGHT - 1))
        m &= (m >> 2 * (BITMAP_HEIGHT - 1))
        if m:
            start = int(math.log(m, 2))
            return 1 + start // BITMAP_HEIGHT, 1 + start % BITMAP_HEIGHT, 1, -1

        # Diagonal /
        m = position & (position >> (BITMAP_HEIGHT + 1))
        m &= (m >> 2 * (BITMAP_HEIGHT + 1))
        if m:
            start = int(math.log(m, 2))
            return 1 + start // BITMAP_HEIGHT, 1 + start % BITMAP_HEIGHT, 1, 1

        # Vertical
        m = position & (position >> 1)
        m &= (m >> 2)
        if m:
            start = int(math.log(m, 2))
            return 1 + start // BITMAP_HEIGHT, 1 + start % BITMAP_HEIGHT, 0, 1

        # Nothing found
        return None

    def player_has_four_connected(self, player: Player) -> bool:
        return self.has_four_connected(self._human if player == Player.HUMAN else self._machine)

    def get_winner(self) -> Union[Player, None]:
        for player in Player:  # type: Player
            if self.player_has_four_connected(player):
                return player

        return None

    def to_string(self, by_row=False) -> str:
        """ Returns a string representing the column oriented traversal of the board, starting from
        the bottom-left corner. Characters are 'h' for a human position, 'm' for a machine position
        and '0' for an empty position.
        """
        human_str = f"{self._human:049b}".replace('1', 'h')
        machine_str = f"{self._machine:049b}".replace('1', 'm')

        def merge(h, m):
            return h if h != '0' else (m if m != '0' else '0')

        merged = ''.join(merge(h, m) for h, m in zip(human_str, machine_str))

        # remove sentinels
        merged = ''.join(
            merged[start + 1: start + 1 + COLUMNS_HEIGHT] for start in range(0, BITMAP_SIZE, BITMAP_HEIGHT)
        )

        # reverse the string to start from the bottom-left corner
        result = merged[::-1]

        if by_row:
            return ''.join(
                ''.join(t) for t in
                list(zip(*zip(*([iter(result)] * 6))))[::-1]
            )
        else:
            return result
