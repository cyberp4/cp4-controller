import logging
from typing import Callable
from enum import Enum, auto
import subprocess
from threading import Timer
import statistics

import pigpio

from .config import ControllerConfiguration
from .actuator import Servo
from .dspin import MovingHeadDriver, DSPinError
from .board import COLUMNS_COUNT, ROWS_COUNT
from .log import LogMgr


class ControllerState(int, Enum):
    INIT = auto()
    READY = auto()
    PLAYING = auto()
    GAME_OVER = auto()
    REFILLING = auto()
    SHUTDOWN = auto()


SHUTDOWN_BUTTON_PRESS_SECONDS = 4


class GPIOCallBack:
    def __call__(self, gpio: int, level: int, ticks: int):
        ...

    def cancel(self):
        ...


TokenPlayedCallback = Callable[[int], None]  # args: column
GateChangedCallback = Callable[[bool], bool]  # args: state
ButtonChangedCallback = Callable[[bool], None]  # args: state
StateChangedCallback = Callable[[ControllerState], None]  # args: state
NoArgCallback = Callable[[], None]


class ControllerError(Exception):
    ...


TICK_WRAP_AROUND = 4_294_967_295


class PulseWidthStatistics:
    values: list[list[int]]
    player: int

    def __init__(self):
        self.reset()

    def reset(self):
        self.player = 0
        self.values = [[], []]

    def add(self, value: int):
        self.values[self.player].append(value)
        self.player = (self.player + 1) % 2

    @property
    def mean(self) -> [float, float]:
        return [statistics.mean(values) for values in self.values] if self else None

    @property
    def stdev(self) -> [float, float]:
        return [statistics.stdev(values) for values in self.values] if len(self.values) >= 2 else None

    @property
    def median(self) -> [float, float]:
        return [statistics.median(values) for values in self.values] if self else None

    @property
    def min(self) -> [int, int]:
        return [min(values) for values in self.values] if self else None

    @property
    def max(self) -> [int, int]:
        return [max(values) for values in self.values] if self else None

    @property
    def count(self) -> [int, int]:
        return [len(values) for values in self.values]

    def __bool__(self) -> bool:
        return all(len(values) > 0 for values in self.values)


class Controller:
    def __init__(self,
                 rpi: pigpio.pi,
                 head_driver: MovingHeadDriver,
                 config: ControllerConfiguration,
                 trace_actuators: bool
                 ):
        self.rpi = rpi
        self.fake_mode = getattr(rpi, "fake", False)
        self.head_driver = head_driver
        self.config = config
        self.at_column = None
        self.at_loading_position = False

        app_logger = LogMgr.app_logger
        self.logger = app_logger.getChild('controller')

        actuators_log_level = logging.DEBUG if trace_actuators else logging.INFO
        logger_loader = app_logger.getChild('loader')
        logger_loader.setLevel(actuators_log_level)
        logger_dropper = app_logger.getChild('dropper')
        logger_dropper.setLevel(actuators_log_level)

        self._state = ControllerState.INIT

        self.token_loader = Servo(rpi, config.loader, logger=logger_loader)
        self.token_dropper = Servo(rpi, config.dropper, logger=logger_dropper)

        self.on_token_played = None
        self.on_gate_changed = None
        self.on_button_pressed = None
        self.on_state_changed = None
        self.on_shutdown_requested = None
        self.on_ready_for_refilling = None

        self.gate_is_opened = False
        self.button_was_pressed = False
        self.button_press_timer = None

        self.token_last_edge_tick = 0
        self.token_last_seen_tick = 0

        self.column_heights = [0] * COLUMNS_COUNT

        self.token_detector_callbacks = self.setup_token_detectors()

        self.rpi.set_pull_up_down(config.gate_sensor_gpio, pigpio.PUD_UP)
        self.rpi.set_glitch_filter(config.gate_sensor_gpio, 10_000)
        self.gate_callback = self.rpi.callback(
            config.gate_sensor_gpio,
            pigpio.EITHER_EDGE,
            self._on_gate_changed
        )

        self.rpi.set_pull_up_down(config.button_gpio, pigpio.PUD_UP)
        self.rpi.set_glitch_filter(config.button_gpio, 10_000)
        self.button_callback = self.rpi.callback(
            config.button_gpio,
            pigpio.EITHER_EDGE,
            self._on_button_changed
        )

        self._pulse_stats = PulseWidthStatistics()

        self.state = ControllerState.READY

    @property
    def state(self) -> ControllerState:
        return self._state

    @state.setter
    def state(self, value: ControllerState):
        if value == self._state:
            return

        self._state = value
        self.logger.debug("state changed to %s", value)
        if self.on_state_changed:
            self.on_state_changed(value)

    def _arm_column_gpio_watchdog(self, gpio: int):
        cfg = self.config.token_detector_config(gpio)
        self.rpi.set_watchdog(gpio, 2 * cfg.detection_max_ms)

    def _cancel_column_gpio_watchdog(self, gpio: int):
        self.rpi.set_watchdog(gpio, 0)

    def _token_added_to_column(self, column_index: int):
        self.logger.info("token added to column %d", column_index + 1)

        self.column_heights[column_index] += 1

        if self.on_token_played:
            self.on_token_played(column_index + 1)

    def _is_full(self, column_index: int) -> bool:
        return self.column_heights[column_index] == ROWS_COUNT

    def _maybe_last_token(self, column_index: int) -> bool:
        return self.column_heights[column_index] == ROWS_COUNT - 1

    def column_is_full(self, col_num: int):
        return self._is_full(col_num - 1)

    def setup_token_detectors(self):
        def get_token_detection_callback(column_index: int):
            column_number = column_index + 1

            def callback(gpio, level, tick):
                self._cancel_column_gpio_watchdog(gpio)

                # ignore any event if the column is already full (maybe some artefact)
                if self._is_full(column_index):
                    self.logger.warning("token detected in an already full column (%d) => discarded", column_number)
                    return

                # we got a timeout, and it could be the last token of a column => it IS really the last one
                if level == pigpio.TIMEOUT and self._maybe_last_token(column_index):
                    self.logger.info("detection timeout in a supposedly full column (%s) => validated", column_number)
                    self._token_added_to_column(column_index)
                    return

                config = self.config.token_detector_config(gpio)

                # The digital output of the fork optical sensor goes high when the token moves the lever
                # so that it cuts the I/R beam
                # (note: this is the opposite of the former reflex sensors based solution)
                if level == pigpio.HIGH:  # token detected
                    self.logger.info("rising edge detected at %d for column %d", tick, column_number)
                    self.token_last_edge_tick = tick

                    if self._maybe_last_token(column_index):
                        # set a watchdog since we'll never get a "gone" detection for the last token
                        self._arm_column_gpio_watchdog(gpio)

                elif level == pigpio.LOW:  # token gone and lever is back to its rest position
                    self.logger.info("falling edge detected at %d for column %d", tick, column_number)
                    elapsed = tick - self.token_last_edge_tick
                    if elapsed < 0:
                        elapsed += TICK_WRAP_AROUND  # handle wrap around of ticks count
                    pulse_ms = elapsed // 1000
                    self.logger.info('token pulse in column %d (width=%d ms)', column_number, pulse_ms)

                    # take the detection into account only if :
                    # - this is not some artefact
                    if config.detection_min_ms <= pulse_ms <= config.detection_max_ms:
                        self.logger.info('+ pulse width in range [%d, %d] -> valid detection',
                                         config.detection_min_ms, config.detection_max_ms
                                         )
                        self._pulse_stats.add(pulse_ms)

                        if self.state == ControllerState.PLAYING:
                            self._token_added_to_column(column_index)
                        else:
                            self.logger.info("not playing a game => board not updated")

                        self.token_last_seen_tick = tick

                    else:
                        self.logger.info('+ outside valid range -> discarded')

                else:  # timeout (maybe somebody is tampering with the detector)
                    self.logger.info('event is a timeout (should not happen but...)')

            return callback

        callbacks = []
        for column_ndx, detector_cfg in enumerate(self.config.token_detectors):
            sensor_gpio = detector_cfg.gpio
            if detector_cfg.token_debounce_ms:
                self.rpi.set_glitch_filter(sensor_gpio, detector_cfg.token_debounce_ms * 1000)
            callbacks.append(
                self.rpi.callback(
                    sensor_gpio,
                    pigpio.EITHER_EDGE,
                    get_token_detection_callback(column_ndx)
                )
            )

        return callbacks

    def _on_gate_changed(self, gpio, _level, _ticks):
        gate_is_opened = self.rpi.read(gpio) == 0
        if gate_is_opened == self.gate_is_opened:
            # ignore false state changes
            return

        # give a chance to the application to intercept the event for implementing a custom logic
        if self.on_gate_changed and not self.on_gate_changed(gate_is_opened):
            return

        if gate_is_opened:
            self.end_game()

            if self._pulse_stats:
                self.logger.info("token detection pulse width statistics:")
                self.logger.info("- count  : %s", self._pulse_stats.count)
                self.logger.info("- min    : %s", self._pulse_stats.min)
                self.logger.info("- max    : %s", self._pulse_stats.max)
                self.logger.info("- mean   : %s", self._pulse_stats.mean)
                self.logger.info("- stdev  : %s", self._pulse_stats.stdev)
                self.logger.info("- median : %s", self._pulse_stats.median)

        else:
            self.prepare_for_tank_refilling()

        # store the new state for future false edges elimination
        self.gate_is_opened = gate_is_opened

    def _on_button_changed(self, gpio, _level, _ticks):
        is_pressed = self.rpi.read(gpio) == 0

        self.logger.info("buttons changed (is_pressed=%s)", is_pressed)

        if is_pressed == self.button_was_pressed:
            # ignore false edge detections
            return

        self.button_was_pressed = is_pressed

        if is_pressed:
            # start tracking long press for shutdown triggering
            self.logger.info("start button state timer for shutdown")
            self.button_press_timer = Timer(SHUTDOWN_BUTTON_PRESS_SECONDS, self.trigger_shutdown)
            self.button_press_timer.start()

        else:
            # the button is released while the long press detection timer is running.
            # => this is a "normal" push and release
            if self.button_press_timer:
                self.button_press_timer.cancel()
                self.button_press_timer = None
                self.logger.info("button state timer canceled")

                self.on_button_pressed(is_pressed)

    def trigger_shutdown(self):
        self.logger.info("shutdown timer expired")

        # "unplug" the timer now that it's expired. This acts as a sentinel
        # for other processing to inform that it's over.
        self.button_press_timer = None

        if self.on_shutdown_requested:
            self.logger.info("invoking configured shutdown callback")
            self.on_shutdown_requested()

        else:
            # if no callback configured, initiate the shutdown directly
            self.logger.info('starting the shutdown sequence from default shutdown callback...')
            subprocess.run("echo heartbeat > /sys/class/leds/led1/trigger ; systemctl poweroff &", shell=True)

    def setup(self):
        try:
            self.head_driver.setup()
        except DSPinError as e:
            self.logger.error('Failed initializing the head driver: %s', e)
            self.shutdown()
            raise
        else:
            self.token_dropper.idle()
            self.logger.info("setup complete")

    def shutdown(self):
        self.logger.info("shutting down")
        self.head_driver.teardown()
        for callback in self.token_detector_callbacks:
            callback.cancel()

        self.state = ControllerState.SHUTDOWN

    def __enter__(self):
        self.setup()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.shutdown()

    def start_game(self):
        self.column_heights = [0] * COLUMNS_COUNT
        self.token_last_edge_tick = self.token_last_seen_tick = 0

        self.go_to_loading_position()

        self._pulse_stats.reset()

        self.state = ControllerState.PLAYING

    def end_game(self):
        self.state = ControllerState.GAME_OVER

    def prepare_for_tank_refilling(self):
        if self.state == ControllerState.GAME_OVER:
            self.go_to_tank_refilling_position()
            self.state = ControllerState.REFILLING

            if self.on_ready_for_refilling:
                self.on_ready_for_refilling()

    def calibrate_moving_head(self):
        self.head_driver.calibrate()
        self.at_loading_position = False
        self.at_column = None

    def go_to_loading_position(self):
        if not self.head_driver.is_calibrated:
            self.calibrate_moving_head()

        self.head_driver.go_to_loading_position()
        self.at_loading_position = True
        self.at_column = None

    def go_to_column(self, which: int):
        self.head_driver.go_to_column(which)
        self.at_column = which

    def go_to_first_column(self):
        self.go_to_column(1)

    def go_to_last_column(self):
        self.go_to_column(COLUMNS_COUNT)

    def go_to_center_column(self):
        self.go_to_column(COLUMNS_COUNT // 2 + 1)

    def load_token(self):
        if not self.at_loading_position:
            raise ControllerError('not at loading position')

        self.token_loader.actuate()

    def drop_token(self, wait=True):
        if self.at_column is None:
            raise ControllerError('not at a column position')
        if self._is_full(self.at_column - 1):
            raise ControllerError('column full')

        self.token_dropper.actuate(wait=wait)

        if self.fake_mode:
            self.logger.info("simulating on_token_played trigger by sensor event")
            self.on_token_played(self.at_column)

    def go_to_tank_refilling_position(self):
        if self.head_driver.is_calibrated:
            self.go_to_column(COLUMNS_COUNT)
        self.calibrate_moving_head()

    def disable_head(self):
        self.head_driver.disable()

    def play_token_in_column(self, which: int):
        """ Go to the loading position (if not already there), load a token, play it at requested column
        and park back to the loading position.

        :param which: column number (in [1, COLUMNS_COUNT])
        """
        self.go_to_loading_position()
        self.load_token()
        self.go_to_column(which)
        self.drop_token()
        self.go_to_loading_position()

    def get_token_detectors_state(self):
        states = [
            self.rpi.read(cfg.gpio) for cfg in self.config.token_detectors
        ]
        return states

    def get_gate_state(self):
        return self.rpi.read(self.config.gate_sensor_gpio)
