from enum import Enum
from typing import Optional

C4Bitmap = int


class Player(Enum):
    HUMAN = ('h', 'human')
    MACHINE = ('m', 'machine')

    def __init__(self, code, label):
        self.code = code
        self.label = label

    def __str__(self):
        return self.label


class SystemEvent(str, Enum):
    SYSTEM_READY = 'system_ready'
    SHUTDOWN_STARTED = 'shutdown_started'
    ERROR = 'error'
    STATE_CHANGED = 'state_changed'
    GAME_STARTED = 'game_started'
    GAME_OVER = 'game_over'
    PLAYER_CHANGED = 'player_changed'
    TOKEN_PLAYED = 'token_played'
    READY_FOR_REFILLING = 'ready_for_refilling'
    TOKEN_DETECTED = 'token_detected'


class Game:
    winner: Optional[Player]

    def __init__(self):
        self.winner = None
