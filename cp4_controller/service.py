import time
from pathlib import Path
from urllib.request import urlopen
from http.client import HTTPResponse
import json
from typing import Union, Tuple
import subprocess

import pika
from pika.adapters.blocking_connection import BlockingChannel
from pika.exceptions import AMQPError

try:
    model = Path("/sys/firmware/devicetree/base/model").read_text()
    is_raspberrypi = "raspberry" in model.lower()
except IOError:
    is_raspberrypi = False

if is_raspberrypi:
    import pigpio
else:
    from . import fake_pigpio as pigpio

from .errors import ServiceControllerError, ServiceAMQPError
from .controller import Controller, ControllerState, ControllerError
from .dspin import MovingHeadDriver, DSPinError
from .config import ControllerConfiguration, ServiceSettings
from .app_types import Player, SystemEvent
from .board import Board, COLUMNS_COUNT
from .log import LogMgr
from .api import CP4HttpServer
from .rpi_leds import LedActivity, LEDMode

AMQP_EXCHANGE = 'cp4'

SHUTDOWN_BUTTON_PRESS_SECONDS = 4


class Service:
    controller: Controller
    server: CP4HttpServer

    def __init__(self,
                 rpi: pigpio.pi,
                 head_driver: MovingHeadDriver,
                 controller_config: ControllerConfiguration,
                 settings: ServiceSettings,
                 ):
        self.rpi = rpi
        self.head_driver = head_driver
        self.config = controller_config
        self.settings = settings

        self.logger = LogMgr.app_logger.getChild('svc')

        self.board = Board()
        self.player = Player.HUMAN
        self.winner: Union[Player, None] = None

        try:
            self.amqp_connection, self.amqp_channel = self.amqp_setup(settings.rmq_username, settings.rmq_password)

        except pika.exceptions.AMQPConnectionError as e:
            raise ServiceAMQPError(e)

    def amqp_setup(self, username: str, password: str) -> Tuple[pika.BlockingConnection, BlockingChannel]:
        self.logger.info('initializing AMQP...')

        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host='localhost',
                credentials=pika.PlainCredentials(username, password),
                heartbeat=0
            )
        )
        channel = connection.channel()
        channel.exchange_declare(exchange=AMQP_EXCHANGE, exchange_type='topic')
        return connection, channel

    def amqp_cleanup(self):
        if self.amqp_connection and self.amqp_connection.is_open:
            self.logger.info('closing AMQP connection...')
            self.amqp_connection.close()
            self.amqp_connection = None
            self.amqp_channel = None

    def shutdown(self):
        self.logger.info("shutting down...")
        LedActivity.restore_default()
        self.amqp_cleanup()

    def run(self):
        try:
            with Controller(
                    self.rpi,
                    head_driver=self.head_driver,
                    config=self.config,
                    trace_actuators=self.settings.trace_actuators
            ) as controller:
                controller.on_token_played = self.on_token_played
                controller.on_gate_changed = self.on_gate_changed
                controller.on_button_pressed = self.on_button_changed
                controller.on_state_changed = self.on_state_changed
                controller.on_shutdown_requested = self.on_shutdown_requested
                controller.on_ready_for_refilling = self.on_ready_for_refilling

                self.controller = controller

                # the API server
                listening_port = self.settings.listening_port
                self.server = CP4HttpServer(
                    listening_port,
                    controller,
                    self.board
                )

                self.head_driver.calibrate(disable_after=True)

                # change RPi activity LED state to show we are now up and ready
                LedActivity.set_mode(LEDMode.HEARTBEAT)

                # and notify the world we are
                self.emit_event(SystemEvent.SYSTEM_READY)

                self.logger.info('API server listening on port %d...', listening_port)
                try:
                    self.server.serve_forever()

                except KeyboardInterrupt:
                    self.logger.info('!! Ctrl-C or SIGINT received !!')

                finally:
                    self.head_driver.disable()

        except DSPinError as e:
            self.logger.error("Cannot start service: %r", e)
            raise ServiceControllerError(e)

        finally:
            LedActivity.restore_default()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.shutdown()

        self.controller = None

    def on_token_played(self, column: int):
        # don't take the detection into account if not playing a game, but instead send a
        # special event used to help calibrating
        if not self.controller.state == ControllerState.PLAYING:
            self.emit_event(SystemEvent.TOKEN_DETECTED, {'column': column})
            return

        self.emit_event(SystemEvent.TOKEN_PLAYED, {'player': self.player.name, 'column': column})

        self.board.update(self.player, column)

        self.logger.info("%s played in column %d", self.player, column)

        # Add some delay before reacting, so that the token has enough time to reach its
        # rest position before something happens.
        # There is no special requirement to do this, but it makes the device behaving
        # a bit more "naturally".
        time.sleep(1)

        if self.board.player_has_four_connected(self.player):
            self.logger.info('%s won', self.player)

            self.controller.end_game()
            self.winner = self.player

            self.emit_event(SystemEvent.GAME_OVER, {'winner': self.player.name})

            if self.winner == Player.MACHINE:
                self.make_winner_dance()

        elif self.board.is_full():
            self.logger.info('game ended with a null')
            self.controller.end_game()
            self.emit_event(SystemEvent.GAME_OVER, {'winner': None})

        else:
            self.change_player()

    def make_winner_dance(self):
        c4 = self.board.get_four_connected(self.winner)
        if c4 is None:
            return

        start_col, _, dir_c, _ = c4
        if dir_c:
            for _ in range(3):
                col = start_col
                for ndx in range(4):
                    self.controller.go_to_column(col)
                    col += dir_c

        else:
            self.controller.go_to_column(start_col)
            time.sleep(3)

        self.controller.go_to_loading_position()

    def on_gate_changed(self, is_opened: bool) -> bool:
        if is_opened:
            LedActivity.set_mode(LEDMode.OFF)

            # notify a game over without including the payload
            # since we can be aborting an unfinished game
            self.emit_event(SystemEvent.GAME_OVER)

        # use the default logic of the controller
        return True

    def change_player(self):
        self.player = Player.MACHINE if self.player == Player.HUMAN else Player.HUMAN
        self.logger.info("%s's turn", self.player)

        self.emit_event(SystemEvent.PLAYER_CHANGED, {'player': self.player.name})

        if self.player == Player.MACHINE:
            # visual clue for machine's turn to play
            LedActivity.set_mode(LEDMode.OFF)

            # tolerate AI to propose unplayable options in case of bug... but not too many
            retries_left = 3

            while retries_left:
                ai_url = f"{self.settings.game_ai_netloc}/move"
                board_string = self.board.to_string()

                self.logger.info("contacting the AI at %s for which column to play next", ai_url)
                self.logger.info("- board string: %s", board_string)

                resp: HTTPResponse = urlopen(f"{ai_url}?b={board_string}", timeout=5)

                if resp.status == 200:
                    column = int(resp.read())
                    self.logger.info("--> replied: %d", column)

                    try:
                        self.controller.play_token_in_column(column)

                    except ControllerError as e:
                        self.logger.error("could not play: %s", e)
                        ai_failure = str(e)
                        retries_left -= 1

                    else:
                        return

                else:
                    self.logger.error("AI error response: %s %s", resp.status, resp.reason)
                    ai_failure = f"{resp.status} {resp.reason}"
                    retries_left -= 1

                if ai_failure:
                    if retries_left:
                        self.logger.info("giving another chance to the AI...")
                    else:
                        self.logger.error("giving up with the AI")
                        self.emit_event(SystemEvent.ERROR, {'detail': ai_failure})

        else:
            # visual clue for human's turn to play
            LedActivity.set_mode(LEDMode.BLINK)

    def on_button_changed(self, is_pressed: bool):
        self.logger.debug("on_button_changed(is_pressed=%s)", is_pressed)

        self.board = Board()
        self.player = Player.HUMAN

        self.controller.start_game()

        self.emit_event(SystemEvent.GAME_STARTED, {'player': self.player.name})

        LedActivity.set_mode(LEDMode.ON)

        return True

    def on_state_changed(self, new_state: ControllerState):
        self.emit_event(SystemEvent.STATE_CHANGED, {'state': new_state.name})

    def on_shutdown_requested(self):
        # park the head next to the tank
        self.controller.go_to_loading_position()

        self.emit_event(SystemEvent.SHUTDOWN_STARTED)

        # let some time for the event to be handled by some listening service, such as the vocal synthesizer
        time.sleep(5)

        if not self.settings.poweroff_dry_run:
            self.logger.info('sending poweroff command')
            subprocess.run(
                "(echo heartbeat > /sys/class/leds/led1/trigger ; sleep 1 ; systemctl poweroff)&",
                shell=True
            )
        else:
            self.logger.warning('poweroff dry-run is set. Not sending the command.')

    def on_ready_for_refilling(self):
        self.emit_event(SystemEvent.READY_FOR_REFILLING)

    def emit_event(self, event: Union[SystemEvent, SystemEvent], payload: dict = None):
        routing_key = str(event.value)
        body = {
            '__time__': time.time()
        }
        if payload:
            body.update(payload)
        self.logger.info("emitting %s (routing key=%s, body=%s)", event, routing_key, body)
        try:
            self.amqp_channel.basic_publish(
                exchange=AMQP_EXCHANGE,
                routing_key=routing_key,
                body=json.dumps(body).encode()
            )
        except AMQPError as e:
            self.logger.error("basic_publish failed: %s", e)
