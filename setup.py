from setuptools import setup
from pathlib import Path

setup(
    name='cp4_controller',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    packages=['cp4_controller'],
    url='',
    license='',
    author='Eric Pascual',
    author_email='',
    description='',
    python_requires='>=3.9.2',
    install_requires=Path('requirements.txt').read_text(),
    entry_points={
        'console_scripts': [
            'cp4-ctrld=cp4_controller.main:main'
        ]
    }
)
