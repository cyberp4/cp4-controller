.ONESHELL:

PROJECT_NAME=cp4_controller

HOST=rpi-p4
REMOTE_PATH=

VERSION=$(shell grep current_version .bumpversion.cfg | cut -d= -f2 | sed 's/ //g')

default: build

.PHONY: lint
lint:
	mypy $(PROJECT_NAME)

.PHONY: build
build:
	@mkdir -p dist/arch
	if test -f dist/*.whl ; then mv dist/*.whl dist/arch ; fi
	python3 setup.py bdist_wheel

.PHONY: deploy
deploy:
	ssh $(HOST) 'rm $(PROJECT_NAME)-*-py3-none-any.whl'
	scp dist/*.whl $(HOST):$(REMOTE_PATH)
	ssh $(HOST) 'pip install $(PROJECT_NAME)-*-py3-none-any.whl --force-reinstall --no-deps --no-warn-script-location'

.PHONY: update-config
update-config:
	scp systemd/cp4-controller.service assets/config.yaml $(HOST):$(REMOTE_PATH)

.PHONY: restart
restart:
	ssh $(HOST) 'sudo systemctl daemon-reload && sudo systemctl restart cp4-controller'

.PHONY: show-version
show-version:
	@echo $(VERSION)
